# a2e

![256x256](assets/icons/png/256x256.png)

The ASCII to ESR converter.

Two modes supported:

- Single file
- Folder (for batch conversion)

![a2electron](a2electron.gif)