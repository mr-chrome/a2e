// From node
const fs = require('fs'); // To work with files
const btn_inpath = document.getElementById("btn-inpath");
const btn_outpath = document.getElementById("btn-outpath");
const btn_convert = document.getElementById("btn-convert");
const code_preview = document.getElementById("preview");
const switch_preview = document.getElementById('switch-preview');
const radio_singlefile = document.getElementById('radio-singlefile');
const radio_folder = document.getElementById('radio-folder');

// Global GUI vars
let lbl_outcome = document.getElementById('lbl-outcome');
let input_inpath = document.getElementById('input-inpath');
let input_outpath = document.getElementById('input-outpath');
let input_date = document.getElementById('input-date');
let input_comment = document.getElementById('input-comment');

// Get stuff
function getDate() {
  if (typeof input_date.value !== 'undefined' && input_date.value !== "") {
    return(String(input_date.value));
    console.log('data acquisita');
  }
  else {
    return('***data sconosciuta***');
    console.log('data sconosciuta');
  }
}

function getComment() {
  if (typeof input_comment.value !== 'undefined' && input_comment.value !== "") {
    return(String(input_comment.value));
    console.log('commento inserito');
  }
  else {
    return('***commento mancante***');
    console.log('commento mancante');
  }
}

function getFilename() {
  // get just filename without path and extension
  if (input_inpath.value.match(/^\S+\S+\.txt$/g)){
    console.log('Bene! Il file è un .txt');
    var inpath_value = input_inpath.value.replace('.txt', '');
    return inpath_value.replace(/^\S+\//g, '');
  }
  else {
    console.log('Il file ha un formato sconosciuto!');
    labelStatus(lbl_outcome, 'Formato sconosciuto: seleziona file .txt', 'error');
  }
}

// Set stuff
function outcomeText(text) {
  // Re-set ready as label text
  while( lbl_outcome.firstChild ) {
    lbl_outcome.removeChild( lbl_outcome.firstChild );
  }
  lbl_outcome.appendChild( document.createTextNode(text));
  console.log('Label Outcome text changed');
}

function slowLabelOut(somelement, someclass, delay) {
  // Add some class and ...
  somelement.classList.add(someclass);

  // ... Removes it with a slow css transition
  setTimeout(function(){
    somelement.classList.add('slowLabelOut');
    somelement.classList.remove(someclass);
    // Remove transition
    setTimeout(function() {
      somelement.classList.remove('slowLabelOut');
    }, delay) // delay in ms
  }, delay); // delay in ms
}

// Edit status of some label in the GUI (text and class)
function labelStatus(label, text, status) {
  outcomeText(text);
  var newclass = 'label-' + status;
  if (status == 'secondary') {
    slowLabelOut(label, newclass, 2000);
  }
  else {
    label.classList.add(newclass);
  }
}

function setPreview(text) {
  while( code_preview.firstChild ) {
    code_preview.removeChild( code_preview.firstChild );
  }
  code_preview.appendChild( document.createTextNode(text) );
  var pre_code = document.getElementById("pre_code");
  var inpath_value = input_inpath.value;
  if (inpath_value.match(/^\S+\S+\.txt$/g)) {
    pre_code.setAttribute('data-lang', 'ASCII');
    // DISPLAY CODE
  }
  else if (inpath_value.match(/^\S+\S+$/g)) {
    pre_code.setAttribute('data-lang', 'Folder content:')
    // DISPLAY LIST OF FILES
  }
  else {
    pre_code.setAttribute('data-lang', '???');
    // DISPLAY SOME ASCII DRAW
  }
}

// Reset
function resetFields() {
  lista = document.getElementsByTagName("input");
  for (var i = 0; i < lista.length; i++) {
    lista[i].value = "";
  }
  lbl_outcome.className = 'label';
  setPreview('')
  filelist = undefined // global list
  str_filelist = undefined // global string list
  console.log('reset fields succeded');
}

// Core functions

function lineStrip(line) {
  // Get ASCII line and return a clean string with numbs separated by a space
  if(typeof(line.trim) !== "undefined")
  {
    var line = String(line).replace(/^\s+|\s+|\s+$/g, ' '); // Clean
    return line.replace(/^\s+|\s+$/g, ''); // Strip;
  }
}

function makeColumn(line) {
  // Make array from a cleaned line string
  if(typeof(line.trim) !== "undefined")
  {
    return line.split(' '); // Make Columns
  }
}

function GenerateColumns(inpath_value) {
  // Genera liste vuote iniziali
  var f = []; // Field list
  var i = []; // Intensity list
  var ni = []; // Intensity with commas long string

  var data = fs.readFileSync(inpath_value, 'utf8');

  // Split the contents by new line
  const lines = data.split(/\r?\n/);

   // for line in lines...
   lines.forEach((line) => {
     if(typeof(line.trim) !== "undefined")
     {
       var line = lineStrip(line); // Strip and Clean line
       var columns = makeColumn(line); // Make array from line

       if (columns.length === 3) {
         f.push(columns[1]);
         i.push(columns[2]);
         ni.push(String(columns[2].concat('\n')));
       }//.if column.length
     }//.if String undefined
   });//.forEach
return([f, i, ni])
}//GenerateColumns

// Make new file
function GenerateESR(inpath_value, outpath_value) {
  var finilist = GenerateColumns(inpath_value);
  f_list = finilist[0];
  i_list = finilist[1];
  ni_string = finilist[2].join('');

  f_points = f_list.length; // Number of Fields points
  points = i_list.length; // Number of spectrum points

  // Compare number of points and get main intensity values
  if (f_points === points && points > 0) {
    var f_start = f_list[0];
    var f_end = f_list[f_points - 1];
  }
  else {
    console.log('ERROR: something wrong with number of points!');
    labelStatus(lbl_outcome, 'Points number error.', 'error') // Show error in the label outcome
    return; // Stop the function
  }

  // Write func
  function write(stringvalue) {
    fs.writeFile(outpath_value, stringvalue, function(err) {
      if(err) {
          return console.log(err);
        }
      console.log("The file was saved!");
    });//.write File
  }//.write

  // Write frontmatter
  var frontmatter =
    '***********************\n' +
    getDate() + '\n' +
    String(f_start).concat('\n') +
    String(f_end).concat('\n') +
    String(points).concat('\n') +
    getComment() + '\n' +
    '***********************\n';

  write(frontmatter + ni_string);
  return;
}//.GenerateESR

function GenerateESRcycle() {
  // Make dir if doesn't exists
  if (!fs.existsSync(input_outpath.value)){
    console.log('Making new folder...');
    fs.mkdirSync(input_outpath.value);
    console.log('... Done!');
  }

  var filelist = [] // global list
  fs.readdirSync(input_inpath.value).forEach(file => {
    if (String(file).endsWith('.txt')) {
      filelist.push(file);
    }
  }); //.forEach

  var filenumber = filelist.length;
  console.log(filenumber + ' files are ready to be processed!');

  for (var i = 0; i < filenumber; i++) {
    var inp = input_inpath.value + '/' + filelist[i];
    var oup = input_outpath.value + '/' + filelist[i].replace('.txt', '.esr');
    GenerateESR(inp, oup);
  }

  return multicounter = i;
}//.GenerateESRcycle

function singleGen() {
  try {
    GenerateESR(input_inpath.value, input_outpath.value);
    console.log('singleGen succeded.');
    labelStatus(lbl_outcome, 'Conversione singola riuscita!', 'success');
  } catch (err) {
    console.log(err);
    console.log('singleGen function failed!');
    labelStatus(lbl_outcome, 'Conversione singola fallita :(', 'error');
  }
}

function multiGen() {
  try {
    GenerateESRcycle();
    console.log('multiGen succeded.');
    labelStatus(lbl_outcome, 'Conversione riuscita x ' + String(multicounter) + '!', 'success');
  } catch (err) {
    console.log(err);
    console.log('multiGen function failed!');
    labelStatus(lbl_outcome, 'Conversione multipla fallita :(', 'error');
  }
}

// Open Files and Dirs
const {dialog} = require('electron').remote; // To work with directories

function selectInpathFile() { // Useful in single file mode
  resetFields();
  dialog.showOpenDialog( {
    properties: ['openFile'],
    filters: [{
      name: 'ASCII txt',
      extensions: ['txt']
    }]
  }).then(result => {
    input_inpath.value = result.filePaths;
    labelStatus(lbl_outcome, 'File successfully imported!', 'secondary');
    input_outpath.value = input_inpath.value.match(/^\S+\S+\./gm) + 'esr'
    setPreview(fs.readFileSync(input_inpath.value, 'utf8')); // Text of file
  }).catch(err => {
    console.log(err)
    console.log('selectInpathFile failed.');
    labelStatus(lbl_outcome, 'File non trovato :(', 'error');
    setPreview('¯\\_(ツ)_/¯')
  })
}

function selectInpathFolder() { // Useful in folder mode
  resetFields();
  dialog.showOpenDialog( {
    properties: ['openDirectory']
  }).then(result => {
    input_inpath.value = result.filePaths;
    input_outpath.value = input_inpath.value.match(/^\S+\S+/gm) + '/esr'
    filelist = [] // Praticamente inutile, array creato dopo dal generatore
    str_filelist = "" // global string list

    fs.readdirSync(input_inpath.value).forEach(file => {
      // Crea lista di file da mostrare nell'anteprima
      if (String(file).endsWith('.txt')) {
        filelist.push(file);
        str_filelist = str_filelist + String(file) + ',\n';
      }
    }); //.forEach

    if (filelist.length > 0) {
      setPreview(str_filelist);
      labelStatus(lbl_outcome, 'File txt trovati', 'secondary');
    }
    else {
      setPreview('¯\\_(ツ)_/¯');
      labelStatus(lbl_outcome, 'File txt non trovati!', 'error');
    }
  }).catch(err => {
    console.log(err)
  })
}

function selectOutpathFile() { // Useful in Single File Mode
  dialog.showOpenDialog( {
    properties: ['openDirectory']
  }).then(result => {
    input_outpath.value = result.filePaths + '/' + getFilename() + '.esr';
  }).catch(err => {
    console.log(err)
  })
}

function selectOutpathFolder() { // Useful in Folder Mode
  dialog.showOpenDialog( {
    properties: ['openDirectory']
  }).then(result => {
    input_outpath.value = result.filePaths;
  }).catch(err => {
    console.log(err)
  })
}

switch_preview.addEventListener('click', function() {
  // See/Unsee the preview
  if (switch_preview.checked === true) {
    pre_code.classList.remove('d-none'); // Display removing display:none
  }
  else {
    pre_code.classList.add('d-none'); // Hide preview
  }
});

// RADIO FORM ACTIONS

if (radio_singlefile.classList.contains('active')) {
  btn_inpath.addEventListener("click", selectInpathFile);
  btn_outpath.addEventListener("click", selectOutpathFile);
  btn_convert.addEventListener('click', singleGen);
}
else if (radio_folder.classList.contains('active')) {
  btn_inpath.addEventListener("click", selectInpathFolder);
  btn_outpath.addEventListener("click", selectOutpathFolder);
  btn_convert.addEventListener('click', multiGen);
}
else {
  labelStatus(lbl_outcome, 'Selezionare una modalità! (Single File / Folder)', 'warning');
}

// RADIO MANAGER

function radioManager(mode) {

  if (mode == 'singlefile') {
    var name = 'Single File';
    var radio_new = radio_singlefile;
    var radio_old = radio_folder;

    var selectInpathNew = selectInpathFile;
    var selectInpathOld = selectInpathFolder;
    var selectOutpathNew = selectOutpathFile;
    var selectOutpathOld = selectOutpathFolder;

    var btn_inpath_html = 'Open File <i class="fa fa-file" aria-hidden="true"></i>';

    btn_convert.removeEventListener("click", multiGen);
    btn_convert.addEventListener("click", singleGen);
  }
  else if (mode == 'folder') {
    var name = 'Folder';
    var radio_new = radio_folder;
    var radio_old = radio_singlefile;

    var selectInpathNew = selectInpathFolder;
    var selectInpathOld = selectInpathFile;
    var selectOutpathNew = selectOutpathFolder;
    var selectOutpathOld = selectOutpathFile;

    var btn_inpath_html = 'Open Folder <i class="fa fa-folder" aria-hidden="true"></i>'

    btn_convert.removeEventListener("click", singleGen);
    btn_convert.addEventListener("click", multiGen);
  }

  if (radio_new.classList.contains('active')) {
    labelStatus(lbl_outcome, 'Modalità ' + name + ' già selezionata!', 'secondary');
  }
  else if (radio_new.classList.contains('not-active')) {
    if (radio_old.classList.contains('active')) {
      radio_old.classList.remove('active');
      radio_old.classList.add('not-active');

      btn_inpath.removeEventListener("click", selectInpathOld);
      btn_outpath.removeEventListener("click", selectOutpathOld);
    }

    radio_new.classList.remove('not-active');
    radio_new.classList.add('active');
    btn_inpath.innerHTML = btn_inpath_html;
    btn_inpath.addEventListener("click", selectInpathNew);
    btn_outpath.addEventListener("click", selectOutpathNew);

    resetFields(); // Resetta i vari form!
    labelStatus(lbl_outcome, 'Modalità ' + name + ' abilitata!', 'secondary');
  }
}//.radioManager

radio_singlefile.addEventListener('click', function() {
  radioManager('singlefile');
});

radio_folder.addEventListener('click', function() {
  radioManager('folder');
});
